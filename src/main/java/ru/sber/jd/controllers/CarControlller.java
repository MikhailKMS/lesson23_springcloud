package ru.sber.jd.controllers;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.jd.dto.CarDto;

import java.util.Random;

@RestController
@RequestMapping("/cars")
public class CarControlller {


    @GetMapping
    public CarDto get() {

        CarDto carDto = new CarDto();
        carDto.setId(new Random().nextInt());
        carDto.setVehicleRegistrationNumber("a777aa52RUS");
        carDto.setCarModel("Mersedes Maybach S600");
        carDto.setCarColor("Deep Black");

        return carDto;

    }


}
