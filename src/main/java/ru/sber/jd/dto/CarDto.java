package ru.sber.jd.dto;

import lombok.Data;

@Data
public class CarDto {

    private Integer id;
    private String vehicleRegistrationNumber;
    private String carModel;
    private String carColor;

}
